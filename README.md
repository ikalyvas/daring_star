# Dream Candies Automation Process

* The tool can be built by the following commands
  * cd to daring_star folder
  * virtualenv venv
  * pip install -r requirement.txt
  * python setup.py install 
    * this command will install a console script
    by the name do_filter_candy
  * invoke the script : 
    *  do_filter_candy -s customer_sample.csv -f customer.csv invoice.csv invoice_item.csv

  * You should normally see 3 files with suffix _small compared to the 3 files given as input to the script with the -f flag.
   * customer_small.csv
   * invoice_small.csv
   * invoice_item_small.csv
   
* There are a couple of tests that show how testing could be done for the tool.
 Note that this is just a sample and not full testing(although it seems the business logic)
 Within the daring_star dir and inside the virtual environment run:
   * nosetests
   