from argparse import ArgumentParser
import argparse
import pandas as pd
import csv


def replace_smart_quotes(df):
    df = df.applymap(lambda x: x.replace('“', '"').replace('”', '"'))
    return df


def create_dataframes_from_cmd(args):

    sample_file = args.sample_file[0]
    full_extract = args.full_extract
    df_customer = None
    df_invoice = None
    df_invoice_item = None

    df_sample = pd.read_csv(sample_file.name)
    df_sample = replace_smart_quotes(df_sample)

    for f in full_extract:
        if f.name == 'customer.csv':
            df_customer = pd.read_csv(f.name)
            df_customer = replace_smart_quotes(df_customer)
        if f.name == 'invoice.csv':
            df_invoice = pd.read_csv(f.name)
            df_invoice = replace_smart_quotes(df_invoice)
        if f.name == 'invoice_item.csv':
            df_invoice_item = pd.read_csv(f.name)
            df_invoice_item = replace_smart_quotes(df_invoice_item)

    return df_sample, df_customer, df_invoice, df_invoice_item


def filter_dfs(df_sample, df_customer, df_invoice, df_invoice_item):

    common_customers = df_customer['“CUSTOMER_CODE”'].isin(df_sample['“CUSTOMER_CODE”'])
    df_customer_found = df_customer[common_customers]
    if df_customer_found.empty:
        print("Empty customer dataframe.No match found")
    df_customer_found.to_csv('customer_small.csv', index=False, quoting=csv.QUOTE_NONE)

    common_invoice = df_invoice['“CUSTOMER_CODE”'].isin(df_sample['“CUSTOMER_CODE”'])
    df_invoice_found = df_invoice[common_invoice]
    if df_invoice_found.empty:
        print("Empty invoice dataframe.No match found")
    df_invoice_found.to_csv('invoice_small.csv', index=False, quoting=csv.QUOTE_NONE)

    existing_invoice_items = df_invoice_item['“INVOICE_CODE”'].isin(df_invoice_found['”INVOICE_CODE”'])
    df_existing_invoice_found = df_invoice_item[existing_invoice_items]
    if df_existing_invoice_found.empty:
        print("Empty invoice item dataframe.No match found")
    df_existing_invoice_found.to_csv('invoice_item_small.csv', index=False, quoting=csv.QUOTE_NONE)


def main():
    parser = ArgumentParser(prog="Dream Candies")
    parser.add_argument('--sample-file', '-s', type=argparse.FileType('r'), nargs=1,
                        help="The sample file that contains 1000 customer samples",
                        metavar="sample-customer-file.csv")
    parser.add_argument('--full-extract', '-f', type=argparse.FileType('r'), nargs=3,
                        help="The files of the daily full extract from Dream Candies",
                        metavar=('customer.csv', 'invoice.csv', 'invoice_item.csv'))
    args = parser.parse_args()

    df_sample, df_cust, df_invoice, df_invoice_items = create_dataframes_from_cmd(args)
    filter_dfs(df_sample, df_cust, df_invoice, df_invoice_items)


if __name__ == '__main__':
    main()