import unittest
import io
from mock import MagicMock, patch
from pandas.testing import assert_frame_equal, assert_series_equal
import pandas as pd
from dream_candies.file_filter import create_dataframes_from_cmd, filter_dfs


class FileFilterTest(unittest.TestCase):

    def setUp(self):

        self.sample_file = io.StringIO()
        self.sample_file.write(u"""“CUSTOMER_CODE”
                              “CUST0000010231”
                              “CUST0000010235”
                            """)

        self.sample_file.name = 'customer_sample.csv'

        self.customer_file = io.StringIO()
        self.customer_file.write(u"""“CUSTOMER_CODE”,”FIRSTNAME”,”LASTNAME”
                                 “CUST0000010231”,”Maria”,”Alba”
                                 “CUST0000010235”,”George”,”Lucas”
                                 “CUST0000010236”,”Giannis”,”Kenobi”
                            """)

        self.customer_file.name = 'customer.csv'

        self.invoice_file = io.StringIO()
        self.invoice_file.write(u"""“CUSTOMER_CODE”,”INVOICE_CODE”,”AMOUNT”,”DATE”
                                “CUST0000010231”,”IN0000001”,”105.50”,”01-Jan-2016”
                                “CUST0000010235”,”IN0000002”,”186.53”,”01-Jan-2016”
                                “CUST0000010231”,”IN0000003”,”14.14”,”01-Feb-2016”
                            """)

        self.invoice_file.name = 'invoice.csv'

        self.invoice_item_file = io.StringIO()
        self.invoice_item_file.write(u""""“INVOICE_CODE”,”ITEM_CODE”,”AMOUNT”,”QUANTITY”
                                     “IN0000001”,”MEIJI”,”75.60”,”100”
                                     “IN0000001”,”POCKY”,”10.40”,”250”
                                     “IN0000001”,”PUCCHO”,”19.50”,”40”
                                     “IN0000002”,”MEIJI”,”113.40”,”150”
                                     “IN0000002”,”PUCCHO”,”73.13”,”150”
                                     “IN0000003”,”POCKY”,”16.64”,”400”
                                     “IN0000003”,”PUCCHO”,”97.50”,”200”
                                     “IN0000004”,”PUCCHO”,”97.50”,”200”
                                     """)

        self.invoice_item_file.name = 'invoice_item.csv'


        self.args = MagicMock()
        self.args.sample_file.__getitem__.return_value = self.sample_file
        self.args.ful_extract.return_value = [self.customer_file, self.invoice_file, self.invoice_item_file]

    def test_create_dfs_from_cmd(self):





        df_sample, df_cust, df_inv, df_inv_item = create_dataframes_from_cmd(self.args)

        expected_df_sample = pd.DataFrame({u"“CUSTOMER_CODE”": ['"'+"CUST0000010231"+'"',
                                                                '"'+"CUST0000010235"+'"'
                                                                ]
                                           }
                                          )

        assert_frame_equal(expected_df_sample, df_sample)


    def test_filter_dfs_common_customers_found(self):

        df_sample = pd.DataFrame({u"“CUSTOMER_CODE”": ['"'+"CUST0000010231"+'"',
                                                       '"'+"CUST0000010235"+'"']
                                  })

        df_customer = pd.DataFrame({u"“CUSTOMER_CODE”": ['"'+"CUST0000010231"+'"',
                                                         '"'+"CUST0000010235"+'"',
                                                         '"'+"CUST0000010236"+'"'],

                                    u"”FIRSTNAME”": ['"'+"George"+'"',
                                                     '"'+"Maria"+'"',
                                                     '"'+"Giannis"+'"'],
                                    u"”LASTNAME”": ['"'+"Lucas"+'"',
                                                    '"'+"Alba"+'"',
                                                    '"'+"Kenobi"+'"']
                                  })

        common_customers = df_customer['“CUSTOMER_CODE”'].isin(df_sample['“CUSTOMER_CODE”'])

        assert_series_equal(common_customers,
                            pd.Series([True, True, False], name="“CUSTOMER_CODE”"))


