from distutils.core import setup

from setuptools import find_packages

test_dependencies = ['nose==1.3.7', 'mock==2.0.0']
setup(
    name='dream_candies',
    version='0.1.1',
    url='',
    license='',
    author='giannis kalyvas',
    author_email='jkalivas@gmail.com',
    description='A tool that helps to speed up the automation process of Daring Star',
    packages = find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    tests_require = test_dependencies,
    test_suite = 'nose.collector',
    include_package_data = True,
    entry_points = {'console_scripts': ['do_filter_candy=dream_candies.file_filter:main']}
)
